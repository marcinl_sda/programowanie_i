### Zadania ###

```
1. Wypisać na ekran następujące ciągi liczb:
    1, 3, 5, 7, 9, 11... 61
    0, 2, 4, 6, 4, 2, 0
    100, 10, 200, 20... 900, 90
    1, 1, 2, 3, 5, 8, 13, 21, 34... (ciąg Fibonacciego)
```
```
2. Rysowanie figur w konsoli:
    *         *         ******    *    *
    **         *        *    *     *  *
    ***         *       *    *      **
    ****         *      *    *      **
    *****         *     *    *     *  *
    ******         *    ******    *    *
   Krzyżyk dla chętnych
```
```
#!java
3. Zamienić miejscami elementy tablicy o indeksach 2 i 4:
    int[] myArray = new int[8];
    myArray[2] = 5;
    myArray[4] = 8;
```
```
4. Wykonać operacje na tablicach (po każdym punkcie wyświetlamy):
    • wypełniamy tablicę kolejnymi wartościami od 1 do 10
               [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    • co drugą liczbę zwiększamy o wartość jej poprzednika
              [0, 1, 2, 5, 4, 9, 6, 13, 8, 17]
    • każdą liczbę parzystą dzielimy przez 2
              [0, 1, 1, 5, 2, 9, 3, 13, 4, 17]
    • sumujemy wszystkie liczby w tablicy, wynik = ?
    • jaka będzie suma dla 100 elementów?
```
```
5. Utworzyć klasę RandomUtils która generuje tablicę wypełnioną losowymi wartościami.
```
